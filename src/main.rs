mod helpers;
mod invidious_json;

use anyhow::Result;
use helpers::*;
use std::env;
use std::fs;


#[tokio::main]
async fn main() -> Result<()> {
    dotenv::dotenv().ok();
    fs::create_dir("./videos").ok();
    let channel_id = env::var("CHANNEL_ID")?;
    let auth_token = env::var("WEB3_STORAGE_TOKEN")?;
    let videos = get_videos(&channel_id).await;
    for video in videos {
        let file_path = download_video(video).await?;
        upload_to_ipfs(&file_path, &auth_token).await?;
    }

    Ok(())
}

