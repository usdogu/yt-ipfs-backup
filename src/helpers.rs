use crate::invidious_json::Video;
use reqwest::{get, Client};
use std::fs::File;
use std::io::copy;
use url::form_urlencoded::byte_serialize;
use std::io::{self, Write};
use w3s::writer::{car, uploader, ChainWrite};

fn url_encode(text: &str) -> String {
    byte_serialize(text.as_bytes()).collect()
}

fn get_file_name(path: &str) -> Option<String> {
    let path = std::path::Path::new(path);
    path.file_name()
        .and_then(|name| name.to_str())
        .and_then(|x| Some(x.to_owned()))
}

pub async fn get_videos(channel_id: &str) -> Vec<Video> {
    get(format!(
        "https://invidious.snopyta.org/api/v1/channels/{channel_id}/videos"
    ))
    .await
    .expect("Can't get videos, check your internet connection")
    .json::<Vec<Video>>()
    .await
    .unwrap()
    .into_iter()
    .filter(|video| !video.premium)
    .collect::<Vec<_>>()
}

pub async fn download_video(video: Video) -> anyhow::Result<String> {
    let client = Client::new();
    let postdata = [
        ("id", video.video_id.as_str()),
        ("title", &url_encode(&video.title)),
        (
            "download_widget",
            "%7B%22itag%22%3A22%2C%22ext%22%3A%22mp4%22%7D",
        ), // {"itag":22,"ext":"mp4"}
    ];
    let resp = client
        .post("https://invidious.snopyta.org/download")
        .form(&postdata)
        .send()
        .await?;
    let filename = "./videos/".to_owned() + &video.title + ".mp4";
    let mut dest = File::create(filename.clone())?;
    let content = resp.text().await?;
    copy(&mut content.as_bytes(), &mut dest)?;
    Ok(filename)
}

pub async fn upload_to_ipfs(path: &str, auth_token: &str) -> anyhow::Result<()> {
    let mut file = File::open(path)?;
    let filename = get_file_name(path).unwrap();

    let uploader = uploader::Uploader::new(
        auth_token.into(),
        filename.clone(),
        uploader::UploadType::Car,
        2,
        None
    );
    let mut car = car::Car::new(filename, 1024 * 1024, uploader);

    io::copy(&mut file, &mut car)?;
    car.flush()?;

    let mut uploader = car.next();
    let results = uploader.finish_results().await?;
    println!("results: {:?}", results[0]);

    Ok(())
}
